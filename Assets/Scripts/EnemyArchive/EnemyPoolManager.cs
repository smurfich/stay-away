﻿using System.Collections.Generic;
using UnityEngine;

namespace EnemyArchive
{
    public class EnemyPoolManager : MonoBehaviour
    {
        [SerializeField] GameObject enemyPrefab;
        [SerializeField] int poolSize = 200;

        List<GameObject> enemies = new List<GameObject>();

        void Awake()
        {
            DontDestroyOnLoad(gameObject);

            SpawnAllEnemies();
        }

        void SpawnAllEnemies()
        {
            for (int i = 0; i < poolSize; i++)
            {
                var enemy = Instantiate(enemyPrefab, transform);
                enemies.Add(enemy);
                enemy.SetActive(false);
            } 
        }

        public void ResetAllEnemies()
        {
            foreach (var enemy in enemies)
            {
                enemy.SetActive(false);
            }
        }

        public GameObject GetEnemyFromPool()
        {
            foreach (var enemy in enemies)
            {
                if (!enemy.activeInHierarchy)
                {
                    enemy.SetActive(true);
                    return enemy.gameObject;
                }
            }

            return null;
        }
    }
}
