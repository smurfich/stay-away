﻿using MoreMountains.NiceVibrations;
using Pathfinding;
using UnityEngine;

namespace EnemyArchive
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] AIDestinationSetter aiDestinationSetter;
        [SerializeField] float damageToSize = 0.25f;

        bool isAlive = true;
        public bool IsAlive => isAlive;
        public float DamageToSize => damageToSize;

        void OnDisable()
        {
            isAlive = true;
            aiDestinationSetter.target = null;
        }

        public void SetTarget(Transform target)
        {
            aiDestinationSetter.target = target;
        }

        public void HandleHoleEntrance()
        {
            MMVibrationManager.Haptic(HapticTypes.SoftImpact);
            isAlive = false;
            gameObject.SetActive(false);
        }
    }
}
