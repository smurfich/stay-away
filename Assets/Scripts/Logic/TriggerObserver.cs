﻿using System;
using DG.Tweening;
using Infrastructure;
using UnityEngine;

namespace Logic
{
    public class TriggerObserver : MonoBehaviour
    {
        public event Action<Collider> TriggerEntered;

        Tween scaleTween;

        void OnTriggerEnter(Collider other) => TriggerEntered?.Invoke(other);

        public void SetScale(float scaleFactor) => transform.SetScale(scaleFactor);

        public void UpdateScale(float endValue, float speed)
        {
            float distance = Mathf.Abs(endValue - transform.localScale.x);
            float duration = distance / speed;

            scaleTween?.Kill();
            scaleTween = transform.DOScale(endValue, duration);
        }
    }
}