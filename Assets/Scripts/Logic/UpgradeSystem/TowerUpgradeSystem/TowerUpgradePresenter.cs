﻿using Framework.Code.Infrastructure.Services.PersistentProgress;
using Infrastructure.Data.Tower;
using Logic.TowerBehaviour;
using UnityEngine;
using Zenject;

namespace Logic.UpgradeSystem.TowerUpgradeSystem
{
	public class TowerUpgradePresenter : MonoBehaviour
	{
		[SerializeField] TowerUpgradeView upgradeView;

		TowerUpgradeModel model;
		TowerDataProcessor towerDataProcessor;
		Tower tower;

		IPersistentProgressService progressService;

		[Inject]
		void Construct(TowerDataProcessor towerDataProcessor, IPersistentProgressService progressService)
		{
			this.towerDataProcessor = towerDataProcessor;
			this.progressService = progressService;
		}

		public void Construct(Tower tower) => this.tower = tower;

		public void Enable()
		{
			model = new TowerUpgradeModel(towerDataProcessor, tower, progressService);
			upgradeView.Enable();
			upgradeView.UpdateDamageUpgradeLevel(1);
			upgradeView.UpdateFrequencyUpgradeLevel(1);

			model.FrequencyUpgraded += OnFrequencyUpgraded;
			model.DamageUpgraded += OnDamageUpgraded;
			upgradeView.FrequencyButtonClicked += OnFrequencyButtonClicked;
			upgradeView.DamageButtonClicked += OnDamageButtonClicked;

			progressService.Progress.Collectables.FireAmountChanged += OnFireAmountChanged;
		}

		void OnFrequencyUpgraded(int frequencyUpgradeLevel)
		{
			upgradeView.UpdateFrequencyUpgradeLevel(frequencyUpgradeLevel + 1);
		}

		void OnDamageUpgraded(int damageUpgradeLevel)
		{
			upgradeView.UpdateDamageUpgradeLevel(damageUpgradeLevel + 1);
		}

		void OnFrequencyButtonClicked()
		{
			model.BuyFrequencyUpgrade();

			SwitchFrequencyUpgradeViewState();
		}

		void OnDamageButtonClicked()
		{
			model.BuyDamageUpgrade();

			SwitchDamageUpgradeViewState();
		}

		void SwitchFrequencyUpgradeViewState()
		{
			if (model.HasFrequencyUpgrades() == false)
			{
				upgradeView.DisableFrequencyCost();
				upgradeView.DisableFrequencyUpgradeButton();
			}
			else
			{
				if (model.CanBuyFrequencyUpgrade())
					upgradeView.EnableFrequencyUpgrade();
				else upgradeView.DisableFrequencyUpgradeButton();

				upgradeView.UpdateFrequencyCost(model.CurrentFrequencyCost);
			}
		}

		void SwitchDamageUpgradeViewState()
		{
			if (model.HasDamageUpgrades() == false)
			{
				upgradeView.DisableDamageCost();
				upgradeView.DisableDamageUpgradeButton();
			}
			else
			{
				if (model.CanBuyDamageUpgrade())
					upgradeView.EnableDamageUpgradeButton();
				else upgradeView.DisableDamageUpgradeButton();

				upgradeView.UpdateDamageCost(model.CurrentDamageCost);
			}
		}

		void OnFireAmountChanged()
		{
			SwitchFrequencyUpgradeViewState();
			SwitchDamageUpgradeViewState();
		}


		public void Disable()
		{
			model.FrequencyUpgraded -= OnFrequencyUpgraded;
			model.DamageUpgraded -= OnDamageUpgraded;
			upgradeView.FrequencyButtonClicked -= OnFrequencyButtonClicked;
			upgradeView.DamageButtonClicked -= OnDamageButtonClicked;
			progressService.Progress.Collectables.FireAmountChanged -= OnFireAmountChanged;

			upgradeView.Disable();
		}
	}
}