﻿using System;
using Infrastructure;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Logic.UpgradeSystem.TowerUpgradeSystem
{
    public class TowerUpgradeView : MonoBehaviour
    {
        public event Action FrequencyButtonClicked;
        public event Action DamageButtonClicked;

        [SerializeField] Image upgradePanel;
        [SerializeField] Button frequencyUpgradeButton;
        [SerializeField] Button damageUpgradeButton;
        [SerializeField] Image frequencyUpgradeImage;
        [SerializeField] Image damageUpgradeImage;
        [SerializeField] TextMeshProUGUI frequencyUpgradeCost;
        [SerializeField] TextMeshProUGUI damageUpgradeCost;
        [SerializeField] TextMeshProUGUI frequencyUpgradeLevel;
        [SerializeField] TextMeshProUGUI damageUpgradeLevel;
        [SerializeField] Image frequencyCostIcon;
        [SerializeField] Image damageCostIcon;
        [SerializeField] Image frequencyRang;
        [SerializeField] Image damageRang;

        [SerializeField] Sprite activeFrequencyUpgradeSprite;
        [SerializeField] Sprite inactiveFrequencyUpgradeSprite;
        [SerializeField] Sprite activeDamageUpgradeSprite;
        [SerializeField] Sprite inactiveDamageUpgradeSprite;

        [SerializeField] Sprite activeRang;
        [SerializeField] Sprite inactiveRang;

        [SerializeField] Sprite activeFire;
        [SerializeField] Sprite inactiveFire;

        public void Enable()
        {
            upgradePanel.gameObject.SetActive(true);
            frequencyUpgradeButton.onClick.AddListener(OnFrequencyUpgradeButtonClicked);
            damageUpgradeButton.onClick.AddListener(OnDamageUpgradeButtonClicked);
        }

        public void UpdateFrequencyUpgradeLevel(int level) => frequencyUpgradeLevel.text = level.ToString();

        public void UpdateDamageUpgradeLevel(int level) => damageUpgradeLevel.text = level.ToString();

        public void EnableFrequencyUpgrade()
        {
            frequencyUpgradeImage.sprite = activeFrequencyUpgradeSprite;
            frequencyUpgradeButton.interactable = true;

            frequencyCostIcon.sprite = activeFire;
            frequencyUpgradeCost.SetColorAlpha(1f);

            frequencyRang.sprite = activeRang;
        }

        public void DisableFrequencyUpgradeButton()
        {
            frequencyUpgradeImage.sprite = inactiveFrequencyUpgradeSprite;
            frequencyUpgradeButton.interactable = false;

            frequencyCostIcon.sprite = inactiveFire;
            frequencyUpgradeCost.SetColorAlpha(0.5f);

            frequencyRang.sprite = inactiveRang;
        }

        public void EnableDamageUpgradeButton()
        {
            damageUpgradeImage.sprite = activeDamageUpgradeSprite;
            damageUpgradeButton.interactable = true;

            damageCostIcon.sprite = activeFire;
            damageUpgradeCost.SetColorAlpha(1f);

            damageRang.sprite = activeRang;
        }

        public void DisableDamageUpgradeButton()
        {
            damageUpgradeImage.sprite = inactiveDamageUpgradeSprite;
            damageUpgradeButton.interactable = false;

            damageCostIcon.sprite = inactiveFire;
            damageUpgradeCost.SetColorAlpha(0.5f);

            damageRang.sprite = inactiveRang;
        }

        public void DisableFrequencyCost() => frequencyUpgradeCost.text = "MAX";
        public void DisableDamageCost() => damageUpgradeCost.text = "MAX";

        public void UpdateFrequencyCost(int cost) => frequencyUpgradeCost.text = cost.ToString();

        public void UpdateDamageCost(int cost) => damageUpgradeCost.text = cost.ToString();

        public void Disable()
        {
            frequencyUpgradeButton.onClick.RemoveListener(OnFrequencyUpgradeButtonClicked);
            damageUpgradeButton.onClick.RemoveListener(OnDamageUpgradeButtonClicked);
            upgradePanel.gameObject.SetActive(false);
        }

        void OnFrequencyUpgradeButtonClicked() => FrequencyButtonClicked?.Invoke();

        void OnDamageUpgradeButtonClicked() => DamageButtonClicked?.Invoke();
    }
}