﻿using System;
using DG.Tweening;
using Infrastructure;
using Logic.UI.TapUpgradeAnimations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Logic.UpgradeSystem.TapUpgradeSystem
{
	public class TapUpgradeView : MonoBehaviour
	{
		public event Action AreaButtonClicked;
		public event Action DamageButtonClicked;

		public event Action PlayButtonClicked;

		[SerializeField] Image upgradePanel;
		[SerializeField] Button playButton;
		[SerializeField] Button areaUpgradeButton;
		[SerializeField] Button damageUpgradeButton;
		[SerializeField] Image areaUpgradeImage;
		[SerializeField] Image damageUpgradeImage;
		[SerializeField] Image areaUpgradeText;
		[SerializeField] Image damageUpgradeText;

		[SerializeField] TextMeshProUGUI areaUpgradeCost;
		[SerializeField] TextMeshProUGUI damageUpgradeCost;
		[SerializeField] TextMeshProUGUI totalCoinsText;

		[SerializeField] Sprite activeAreaUpgradeButtonSprite;
		[SerializeField] Sprite inactiveAreaUpgradeButtonSprite;
		[SerializeField] Sprite activeAreaUpgradeImageSprite;
		[SerializeField] Sprite inactiveAreaUpgradeImageSprite;
		[SerializeField] Sprite activeAreaUpgradeTextSprite;
		[SerializeField] Sprite inactiveAreaUpgradeTextSprite;

		[SerializeField] Sprite activeDamageUpgradeButtonSprite;
		[SerializeField] Sprite inactiveDamageUpgradeButtonSprite;
		[SerializeField] Sprite activeDamageUpgradeImageSprite;
		[SerializeField] Sprite inactiveDamageUpgradeImageSprite;
		[SerializeField] Sprite activeDamageUpgradeTextSprite;
		[SerializeField] Sprite inactiveDamageUpgradeTextSprite;

		[SerializeField] ImageAnimator areaImageAnimator;
		[SerializeField] ImageAnimator damageImageAnimator;
		
		public void UpdateTotalCoinsText(int totalCoins) => totalCoinsText.text = totalCoins.ToString();

		public void Enable()
		{
			upgradePanel.gameObject.SetActive(true);
			playButton.onClick.AddListener(OnPlayButtonClick);
			areaUpgradeButton.onClick.AddListener(OnUpgradeAreaButtonClick);
			damageUpgradeButton.onClick.AddListener(OnUpgradeDamageButtonClick);
		}

		public void EnableAreaUpgradeButton()
		{
			areaUpgradeButton.interactable = true;
			areaUpgradeButton.image.sprite = activeAreaUpgradeButtonSprite;
			areaUpgradeImage.sprite = activeAreaUpgradeImageSprite;
			areaUpgradeText.sprite = activeAreaUpgradeTextSprite;
			areaUpgradeCost.SetColorAlpha(1f);
		}

		public void DisableAreaUpgradeButton()
		{
			areaUpgradeButton.interactable = false;
			areaUpgradeButton.image.sprite = inactiveAreaUpgradeButtonSprite;
			areaUpgradeImage.sprite = inactiveAreaUpgradeImageSprite;
			areaUpgradeText.sprite = inactiveAreaUpgradeTextSprite;
			areaUpgradeCost.SetColorAlpha(0.5f);
		}

		public void EnableDamageUpgradeButton()
		{
			damageUpgradeButton.interactable = true;
			damageUpgradeButton.image.sprite = activeDamageUpgradeButtonSprite;
			damageUpgradeImage.sprite = activeDamageUpgradeImageSprite;
			damageUpgradeText.sprite = activeDamageUpgradeTextSprite;
			damageUpgradeCost.SetColorAlpha(1f);
		}

		public void DisableDamageUpgradeButton()
		{
			damageUpgradeButton.interactable = false;
			damageUpgradeButton.image.sprite = inactiveDamageUpgradeButtonSprite;
			damageUpgradeImage.sprite = inactiveDamageUpgradeImageSprite;
			damageUpgradeText.sprite = inactiveDamageUpgradeTextSprite;
			damageUpgradeCost.SetColorAlpha(0.5f);
		}

		public void UpdateAreaCost(int cost) => areaUpgradeCost.text = cost.ToString();
		public void UpdateDamageCost(int cost) => damageUpgradeCost.text = cost.ToString();

		public void DisableAreaCost() => areaUpgradeCost.text = "MAX";
		public void DisableDamageCost() => damageUpgradeCost.text = "MAX";

		public void Disable()
		{
			playButton.onClick.RemoveListener(OnPlayButtonClick);
			areaUpgradeButton.onClick.RemoveListener(OnUpgradeAreaButtonClick);
			damageUpgradeButton.onClick.RemoveListener(OnUpgradeDamageButtonClick);
			upgradePanel.gameObject.SetActive(false);
		}


		void OnPlayButtonClick() => PlayButtonClicked?.Invoke();

		void OnUpgradeAreaButtonClick()
		{
			AreaButtonClicked?.Invoke();
			areaImageAnimator.Animate();
		}

		void OnUpgradeDamageButtonClick()
		{
			DamageButtonClicked?.Invoke();
			damageImageAnimator.Animate();
		}
	}
}