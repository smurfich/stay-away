﻿using System;
using Control;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Infrastructure;
using Infrastructure.Data.PlayerTap;

namespace Logic.UpgradeSystem.TapUpgradeSystem
{
	public class TapUpgradeModel
	{
		public event Action<int> AreaRadiusUpgraded;
		public event Action<int> DamageUpgraded;

		readonly PlayerTapDataProcessor dataProcessor;
		readonly IPersistentProgressService progressService;
		readonly TouchDetector touchDetector;


		float currentAreaRadius;
		float currentDamage;

		PlayerTapData tapData;

		public TapUpgradeModel(PlayerTapDataProcessor dataProcessor, IPersistentProgressService progressService,
			TouchDetector touchDetector)
		{
			this.dataProcessor = dataProcessor;
			this.progressService = progressService;
			this.touchDetector = touchDetector;

			tapData = this.dataProcessor.Load();
		}

		public int CurrentAreaCost { get; private set; }
		public int CurrentDamageCost { get; private set; }

		public void BuyAreaUpgrade()
		{
			if (CanBuyAreaUpgrade() == false) return;

			progressService.Progress.Collectables.TakeAwayCoins(CurrentAreaCost);

			touchDetector.UpdateAreaRadius(currentAreaRadius);
			//touchDetector.RefreshTapSpriteRadius(currentAreaRadius);

			tapData.AreaValue = currentAreaRadius;

			progressService.Progress.TapUpgrade.AreaUpgrade++;

			AreaRadiusUpgraded?.Invoke(progressService.Progress.TapUpgrade.AreaUpgrade);
		}

		public void BuyDamageUpgrade()
		{
			if (CanBuyDamageUpgrade() == false) return;

			progressService.Progress.Collectables.TakeAwayCoins(CurrentDamageCost);

			touchDetector.UpdateDamage(currentDamage);

			tapData.DamageValue = currentDamage;

			progressService.Progress.TapUpgrade.DamageUpgrade++;

			DamageUpgraded?.Invoke(progressService.Progress.TapUpgrade.DamageUpgrade);
		}

		public bool CanBuyAreaUpgrade()
		{
			UpgradeData areaUpgradeData =
				dataProcessor.GetAreaUpgradeData(progressService.Progress.TapUpgrade.AreaUpgrade + 1);

			currentAreaRadius = areaUpgradeData.Value;
			CurrentAreaCost = areaUpgradeData.UpgradeCost;

			return progressService.Progress.Collectables.Amount >= CurrentAreaCost;
		}

		public bool CanBuyDamageUpgrade()
		{
			UpgradeData damageUpgradeData =
				dataProcessor.GetDamageUpgradeData(progressService.Progress.TapUpgrade.DamageUpgrade + 1);

			currentDamage = damageUpgradeData.Value;
			CurrentDamageCost = damageUpgradeData.UpgradeCost;

			return progressService.Progress.Collectables.Amount >= CurrentDamageCost;
		}

		public bool HasAreaUpgrades()
		{
			UpgradeData frequencyUpgradeData =
				dataProcessor.GetAreaUpgradeData(progressService.Progress.TapUpgrade.AreaUpgrade + 1);
			return frequencyUpgradeData.Value > 0;
		}

		public bool HasDamageUpgrades()
		{
			UpgradeData damageUpgradeData =
				dataProcessor.GetDamageUpgradeData(progressService.Progress.TapUpgrade.DamageUpgrade + 1);
			return damageUpgradeData.Value > 0;
		}
	}
}