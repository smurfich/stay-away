﻿using System;
using Control;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Infrastructure.Data.PlayerTap;
using UnityEngine;
using Zenject;

namespace Logic.UpgradeSystem.TapUpgradeSystem
{
	public class TapUpgradePresenter : MonoBehaviour
	{
		public event Action PlayButtonClicked;

		[SerializeField] TapUpgradeView view;

		TapUpgradeModel model;

		PlayerTapDataProcessor dataProcessor;
		IPersistentProgressService progressService;
		TouchDetector touchDetector;

		public void Construct(TouchDetector touchDetector)
		{
			this.touchDetector = touchDetector;
		}

		[Inject]
		void Construct(IPersistentProgressService progressService, PlayerTapDataProcessor dataProcessor)
		{
			this.progressService = progressService;
			this.dataProcessor = dataProcessor;
		}

		public void Enable()
		{
			model = new TapUpgradeModel(dataProcessor, progressService, touchDetector);
			view.Enable();

			view.PlayButtonClicked += OnPlayButtonClicked;
			view.AreaButtonClicked += OnAreaButtonClicked;
			view.DamageButtonClicked += OnDamageButtonClicked;
			progressService.Progress.Collectables.AmountChanged += OnAmountChanged;

			view.UpdateTotalCoinsText(progressService.Progress.Collectables.Amount);

			SwitchAreaUpgradeViewState();
			SwitchDamageUpgradeViewState();
		}

		void OnPlayButtonClicked() => PlayButtonClicked?.Invoke();

		void OnAreaButtonClicked()
		{
			model.BuyAreaUpgrade();
			SwitchAreaUpgradeViewState();

			view.UpdateTotalCoinsText(progressService.Progress.Collectables.Amount);
		}

		void OnDamageButtonClicked()
		{
			model.BuyDamageUpgrade();
			SwitchDamageUpgradeViewState();

			view.UpdateTotalCoinsText(progressService.Progress.Collectables.Amount);
		}

		void OnAmountChanged()
		{
			SwitchAreaUpgradeViewState();
			SwitchDamageUpgradeViewState();
		}

		void SwitchAreaUpgradeViewState()
		{
			if (model.HasAreaUpgrades() == false)
			{
				view.DisableAreaCost();
				view.DisableAreaUpgradeButton();
			}
			else
			{
				if (model.CanBuyAreaUpgrade())
					view.EnableAreaUpgradeButton();
				else view.DisableAreaUpgradeButton();

				view.UpdateAreaCost(model.CurrentAreaCost);
			}
		}

		void SwitchDamageUpgradeViewState()
		{
			if (model.HasDamageUpgrades() == false)
			{
				view.DisableDamageCost();
				view.DisableDamageUpgradeButton();
			}
			else
			{
				if (model.CanBuyDamageUpgrade())
					view.EnableDamageUpgradeButton();
				else view.DisableDamageUpgradeButton();

				view.UpdateDamageCost(model.CurrentDamageCost);
			}
		}

		public void Disable()
		{
			view.PlayButtonClicked -= OnPlayButtonClicked;
			view.AreaButtonClicked -= OnAreaButtonClicked;
			view.DamageButtonClicked -= OnDamageButtonClicked;
			progressService.Progress.Collectables.AmountChanged -= OnAmountChanged;

			view.Disable();
		}
	}
}