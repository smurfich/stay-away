﻿using System;
using DG.Tweening;
using Enemies;
using Enemies.Visitor;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using UnityEngine;
using Zenject;

namespace Logic
{
	public class Bullet : DamageDealer
	{
		public event Action<Bullet> Destroyed;

		[SerializeField] new Rigidbody rigidbody;
		[SerializeField] new Collider collider;
		[SerializeField] ParticleSystem trail;
		[SerializeField] float forcePower;

		IPersistentProgressService progressService;

		float damage;
		Vector3 startTrailPosition;
		Tween disableBulletTween;

		[Inject]
		void Construct(IPersistentProgressService progressService)
		{
			this.progressService = progressService;
		}

		public void Construct(float damage) => this.damage = damage;

		public void Fire(Vector3 direction) => rigidbody.AddForce(direction * forcePower, ForceMode.Impulse);

		public void ResetVelocity()
		{
			rigidbody.velocity = Vector3.zero;
			rigidbody.angularVelocity = Vector3.zero;
		}

		public void ResetCollider()
		{
			collider.enabled = true;
			rigidbody.isKinematic = false;
		}

		void Start() => startTrailPosition = trail.transform.localPosition;

		void OnTriggerEnter(Collider other)
		{
			var enemyHealth = other.GetComponent<EnemyHealth>();
			if (enemyHealth == null) return;

			enemyHealth.TakeDamage(damage, DamageDealerType.Bullet);
			if (enemyHealth.IsDead)
				progressService.Progress.Collectables.AddFireCoins(enemyHealth.GetComponent<EnemyReward>().Reward);

			DisableWithDelay();
		}

		void DetachTrail()
		{
			trail.transform.SetParent(null);

			collider.enabled = false;

			Destroyed?.Invoke(this);
		}

		void DisableWithDelay()
		{
			collider.enabled = false;
			rigidbody.isKinematic = true;
			disableBulletTween = DOVirtual.DelayedCall(1f, () => Destroyed?.Invoke(this));
		}

		public override void Accept(IEnemiesParticlesVisitor particlesVisitor) => particlesVisitor.Visit(this);
	}
}