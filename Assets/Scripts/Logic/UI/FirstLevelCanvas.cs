﻿using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Logic.UI
{
	public class FirstLevelCanvas : MonoBehaviour
	{
		[SerializeField] TextMeshProUGUI tapToStart;
		[SerializeField] TextMeshProUGUI tapOnTheFire;
		[SerializeField] float hintTime;

		public void DisableTapToStartText() => tapToStart.gameObject.SetActive(false);

		public void EnableTapOnTheFireText()
		{
			tapOnTheFire.gameObject.SetActive(true);
			DOVirtual.DelayedCall(hintTime, () => tapOnTheFire.DOFade(0f, 0.8f));
		}
	}
}