﻿using TMPro;
using UnityEngine;

namespace Logic.UI
{
	public class TotalCoinsPassiveView : MonoBehaviour
	{
		[SerializeField] TextMeshProUGUI[] levelRewardText;
		[SerializeField] TextMeshProUGUI[] totalRewardText;

		public void UpdateLevelReward(int reward)
		{
			foreach (TextMeshProUGUI text in levelRewardText)
			{
				if (text.gameObject.activeSelf)
					text.text = reward.ToString();
			}
		}

		public void UpdateTotalRewardText(int reward)
		{
			foreach (TextMeshProUGUI text in totalRewardText)
			{
				if (text.gameObject.activeSelf)
					text.text = reward.ToString();
			}
		}
	}
}