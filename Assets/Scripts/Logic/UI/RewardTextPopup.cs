﻿using DG.Tweening;
using Enemies;
using Infrastructure;
using Infrastructure.ObjectPool.Enemies;
using TMPro;
using UnityEngine;
using Zenject;

namespace Logic.UI
{
	public class RewardTextPopup : MonoBehaviour
	{
		[SerializeField] TextMeshProUGUI rewardText;
		[SerializeField] float yOffset;
		[SerializeField] float endPoint;

		new Camera camera;
		IEnemyPool enemyPool;
		Tween fadeTween;
		Tween moveTween;
		Vector3 startTextPosition;

		[Inject]
		void Construct(IEnemyPool enemyPool)
		{
			this.enemyPool = enemyPool;
		}

		void Start()
		{
			camera = Camera.main;
			startTextPosition = rewardText.transform.position;
			rewardText.gameObject.SetActive(false);

			enemyPool.EnemyDied += OnEnemyDied;
		}

		void OnEnemyDied(GameObject enemy)
		{
			rewardText.SetColorAlpha(1f);
			
			rewardText.gameObject.SetActive(true);
			rewardText.text = $"+{enemy.GetComponent<EnemyReward>().Reward}";

			rewardText.transform.position =
				camera.WorldToScreenPoint(enemy.transform.position) + new Vector3(0f, yOffset, 0f);

			fadeTween?.Kill();
			moveTween?.Kill();

			fadeTween = rewardText.DOFade(0f, 0.8f).SetEase(Ease.OutQuad)
				.OnComplete(() => rewardText.SetColorAlpha(1f));
			moveTween = rewardText.transform.DOMoveY(rewardText.transform.position.y + endPoint, 0.8f).OnComplete(() =>
			{
				rewardText.gameObject.SetActive(false);
			});
		}

		void OnDestroy()
		{
			enemyPool.EnemyDied -= OnEnemyDied;
		}
	}
}