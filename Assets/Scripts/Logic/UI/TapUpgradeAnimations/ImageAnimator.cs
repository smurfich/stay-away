﻿using DG.Tweening;
using Infrastructure;
using UnityEngine;

namespace Logic.UI.TapUpgradeAnimations
{
	public class ImageAnimator : MonoBehaviour
	{
		[SerializeField] ImageAnimation[] arrowAnimations;

		void Start()
		{
			foreach (ImageAnimation arrowAnimation in arrowAnimations)
			{
				arrowAnimation.StartPosition = arrowAnimation.ArrowImage.transform.position;
				arrowAnimation.ArrowImage.gameObject.SetActive(false);
			}
		}

		public void Animate()
		{
			foreach (ImageAnimation arrowAnimation in arrowAnimations)
			{
				arrowAnimation.ArrowImage.gameObject.SetActive(true);
				arrowAnimation.MoveTween?.Kill();
				arrowAnimation.FadeTween?.Kill();
				arrowAnimation.ArrowImage.transform.position = arrowAnimation.StartPosition;
				arrowAnimation.ArrowImage.SetColorAlpha(1f);

				arrowAnimation.MoveTween =
					arrowAnimation.ArrowImage.transform.DOMove(
						arrowAnimation.ArrowImage.transform.position + arrowAnimation.ArrowImage.transform.up * arrowAnimation.EndPoint, 1.5f);
				arrowAnimation.FadeTween = arrowAnimation.ArrowImage.DOFade(0f, 0.8f).SetEase(Ease.OutQuad);
			}
		}
	}
}