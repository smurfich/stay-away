﻿using System.Collections;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Framework.Code.Infrastructure.States;
using Infrastructure;
using Infrastructure.Services.Factories.Particles;
using PlayerZone;
using UnityEngine;
using Zenject;

namespace Logic
{
	public class LevelWinCondition : MonoBehaviour
	{
		Zone zone;
		int reward;
		float targetZoneRadius;
		float deathZoneRadius;

		GameStateMachine stateMachine;
		IPersistentProgressService progressService;
		IParticleFactory particleFactory;
		ILevelEnder[] levelEnders;

		bool isLevelEnded;

		public void Construct(Zone zone, int reward, float targetZoneRadius, float deathZoneRadius,
			ILevelEnder[] levelEnders)
		{
			this.zone = zone;
			this.reward = reward;
			this.targetZoneRadius = targetZoneRadius;
			this.deathZoneRadius = deathZoneRadius;
			this.levelEnders = levelEnders;

			this.targetZoneRadius *= Config.ScaleCoefficient;
			this.deathZoneRadius *= Config.ScaleCoefficient;
		}

		[Inject]
		void Construct(GameStateMachine stateMachine, IPersistentProgressService progressService,
			IParticleFactory particleFactory)
		{
			this.stateMachine = stateMachine;
			this.progressService = progressService;
			this.particleFactory = particleFactory;
		}

		public void Enable()
		{
			zone.SizeChanged += OnZoneSizeChanged;
		}

		void OnZoneSizeChanged(float currentRadius)
		{
			if (isLevelEnded) return;

			if (currentRadius >= targetZoneRadius)
			{
				isLevelEnded = true;
				progressService.Progress.Collectables.Add(reward);
				zone.DisableInnerCircle();

				StartCoroutine(WaitForLevelEnding());
			}

			if (currentRadius <= deathZoneRadius)
			{
				isLevelEnded = true;
				progressService.Progress.Collectables.Add(reward / 2);
				stateMachine.Enter<LoseState>();
			}
		}

		IEnumerator WaitForLevelEnding()
		{
			foreach (ILevelEnder levelEnder in levelEnders)
			{
				levelEnder.EndLevel();
			}

			yield return new WaitForSeconds(Config.FireworkDelay);
			ParticleSystem confettiParticle = particleFactory.CreateConfettiParticle();
			confettiParticle.transform.SetParent(transform.root);

			yield return new WaitForSeconds(Config.SwitchStateDelay);

			stateMachine.Enter<WinState>();
		}

		public void Disable() => zone.SizeChanged -= OnZoneSizeChanged;
	}
}