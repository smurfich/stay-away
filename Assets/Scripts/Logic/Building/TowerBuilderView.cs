﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Logic.Building
{
    public class TowerBuilderView : MonoBehaviour
    {
        public event Action BuyButtonClicked;

        [SerializeField] Image towerBuyPanel;
        [SerializeField] Button towerBuyButton;


        bool isDisabled;

        public void Enable()
        {
            towerBuyPanel.gameObject.SetActive(true);
            towerBuyButton.onClick.AddListener(BuyTower);
        }

        void BuyTower()
        {
            BuyButtonClicked?.Invoke();
            towerBuyButton.interactable = false;
            towerBuyPanel.gameObject.SetActive(false);
        }

        public void Disable()
        {
            if (isDisabled) return;

            isDisabled = true;

            towerBuyButton.onClick.RemoveListener(BuyTower);
            towerBuyPanel.gameObject.SetActive(false);
        }
    }
}