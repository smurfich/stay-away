﻿using DG.Tweening;
using Enemies;
using Enemies.Visitor;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Framework.Code.Infrastructure.States;
using Infrastructure;
using Infrastructure.ObjectPool.Particles;
using Lean.Touch;
using Logic;
using UnityEngine;
using Zenject;

namespace Control
{
	public class TouchDetector : DamageDealer
	{
		const string EnemyLayerName = "Enemy";
		const string GroundLayerName = "Ground";

		[SerializeField] Camera fingerCamera;
		[SerializeField] SpriteRenderer tapSprite;

		Vector3 worldPosition;

		Tween fadeTween;

		IPersistentProgressService progressService;
		IParticlePool particlePool;
		GameStateMachine stateMachine;

		ParticleSystem tapParticle;

		float currentAreaRadius;
		float currentDamage;

		[Inject]
		void Construct(IPersistentProgressService progressService, IParticlePool particlePool,
			GameStateMachine stateMachine)
		{
			this.progressService = progressService;
			this.particlePool = particlePool;
			this.stateMachine = stateMachine;
		}

		public void Construct(float currentAreaRadius, float currentDamage)
		{
			this.currentAreaRadius = currentAreaRadius;
			this.currentDamage = currentDamage;
		}

		public void Enable()
		{
			tapParticle = particlePool.GetTapParticle();

			RefreshTapSpriteRadius(currentAreaRadius);

			LeanTouch.OnFingerDown += OnFingerTap;
		}

		public void Disable()
		{
			LeanTouch.OnFingerDown -= OnFingerTap;

			fadeTween?.Kill(true);
		}

		void RefreshTapSpriteRadius(float radius)
		{
			tapSprite.transform.SetScale(radius * 2);

			if (tapParticle == null)
				tapParticle = particlePool.GetTapParticle();

			tapParticle.transform.SetScale(radius * 2);
		}


		public void UpdateDamage(float damage) => currentDamage = damage;
		public void UpdateAreaRadius(float radius) => currentAreaRadius = radius;

		void OnFingerTap(LeanFinger finger)
		{
			if (stateMachine.ActiveState is WinState || stateMachine.ActiveState is LoseState) return;
			if (finger.IsOverGui) return;

			Vector3 fingerCameraPosition = fingerCamera.transform.position;

			worldPosition = fingerCamera.ScreenToWorldPoint(new Vector3(finger.ScreenPosition.x,
				finger.ScreenPosition.y,
				fingerCameraPosition.y));

			RaycastHit[] touchHits = Physics.RaycastAll(fingerCameraPosition, worldPosition - fingerCameraPosition,
				100f,
				LayerMask.GetMask(GroundLayerName));

			if (touchHits.Length > 0)
			{
				worldPosition = touchHits[0].point;
			}

			RaycastHit[] hits = Physics.SphereCastAll(fingerCameraPosition, currentAreaRadius,
				worldPosition - fingerCameraPosition, 100f, LayerMask.GetMask(EnemyLayerName));

			foreach (RaycastHit hit in hits)
			{
				var enemy = hit.collider.GetComponent<EnemyHealth>();
				enemy.TakeDamage(currentDamage, DamageDealerType.Touch);

				if (enemy.IsDead)
				{
					int enemyReward = enemy.GetComponent<EnemyReward>().Reward;

					progressService.Progress.Collectables.AddFireCoins(enemyReward);
				}
			}

			EnableTouchCircle();
			PlayParticle();
		}

		void PlayParticle()
		{
			if (tapSprite.gameObject.activeInHierarchy == false) return;
			Vector3 tapPosition = tapSprite.transform.position;
			//tapPosition.y -= 0.7f;

			tapParticle.transform.position = tapPosition;
			tapParticle.Play();
		}

		void EnableTouchCircle()
		{
			tapSprite.gameObject.SetActive(true);
			tapSprite.color = new Color(tapSprite.color.r, tapSprite.color.g, tapSprite.color.b, 1f);
			tapSprite.transform.position = worldPosition + new Vector3(0f, 1f, 0f);

			fadeTween?.Kill();
			fadeTween = tapSprite.DOFade(0, 1f).OnComplete(() => tapSprite.gameObject.SetActive(false));
		}

		public override void Accept(IEnemiesParticlesVisitor particlesVisitor) => particlesVisitor.Visit(this);
	}
}