﻿using Infrastructure;
using Logic;
using ProceduralPrimitivesUtil;
using UnityEngine;

namespace PlayerZone
{
    public class ZoneBase : MonoBehaviour
    {
        [SerializeField] Tube tube;
        
        public void SetRadius(float radius)
        {
            tube.radius1 = radius * Config.PrimitivesScaleCoefficient;
            tube.Apply();
            
            
        }
    }
}