﻿using UnityEngine;

namespace Enemies
{
    public class EnemyMovement : MonoBehaviour
    {
        static readonly int Speed = Animator.StringToHash("Speed");
        
        [SerializeField] new Rigidbody rigidbody;
        [SerializeField] Animator animator;

        float speed;
        Vector3 movementDirection;
        Transform target;

        public void Construct(float speed, Transform target)
        {
            this.speed = speed / 4;
            this.target = target;
        }

        void FixedUpdate()
        {
            if (target == null) return;

            movementDirection = (target.position - transform.position).normalized;
            rigidbody.velocity = movementDirection * speed;

            animator.SetFloat(Speed, speed);

            transform.LookAt(movementDirection);
        }


        public void Stop()
        {
            target = null;
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            animator.SetFloat(Speed, 0f);
        }
    }
}