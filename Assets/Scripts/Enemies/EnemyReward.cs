﻿using DG.Tweening;
using Logic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Enemies
{
	public class EnemyReward : MonoBehaviour, ILevelEnder
	{
		[SerializeField] GameObject rewardPopupPrefab;
		[SerializeField] EnemyHealth enemyHealth;
		[SerializeField] float yOffset;
		[SerializeField] float endPoint;

		public int Reward { get; private set; }

		Transform parentCanvas;
		Tween moveTween;
		Tween textFadeTween;
		Tween iconFadeTween;
		Camera mainCamera;

		bool isLevelEnded;

		public void Construct(int reward, Transform parentCanvas)
		{
			Reward = reward;
			this.parentCanvas = parentCanvas;
		}

		void Start()
		{
			mainCamera = Camera.main;
			enemyHealth.FullHealthDamageTaken += OnDeath;
		}

		void OnDeath(DamageDealerType damageDealerType)
		{
			if (isLevelEnded) return;
			if (damageDealerType == DamageDealerType.Zone) return;
			
			GameObject popup = Instantiate(rewardPopupPrefab);

			var rewardText = popup.GetComponentInChildren<TextMeshProUGUI>();
			var fireCoinImage = popup.GetComponentInChildren<Image>();

			rewardText.text = $"+{Reward}";

			popup.transform.SetParent(parentCanvas);
			popup.transform.position =
				mainCamera.WorldToScreenPoint(transform.position) + new Vector3(0f, yOffset, 0f);

			textFadeTween?.Kill();
			iconFadeTween?.Kill();
			moveTween?.Kill();

			textFadeTween = rewardText.DOFade(0f, 0.8f).SetEase(Ease.OutQuad);
			iconFadeTween = fireCoinImage.DOFade(0f, 0.8f).SetEase(Ease.OutQuad);
			moveTween = popup.transform.DOMoveY(rewardText.transform.position.y + endPoint, 1.2f)
				.OnComplete(() => popup.gameObject.SetActive(false));
		}

		void OnDestroy()
		{
			enemyHealth.FullHealthDamageTaken -= OnDeath;
		}

		public void EndLevel()
		{
			isLevelEnded = true;
		}
	}
}