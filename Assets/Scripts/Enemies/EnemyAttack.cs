﻿using UnityEngine;

namespace Enemies
{
    public class EnemyAttack : MonoBehaviour
    {
        [SerializeField] EnemyHealth enemyHealth;

        public float Damage { get; private set; }

        float damageCoefficient;

        public void Construct(float damage)
        {
            Damage = damage;
        }

        public void Enable()
        {
            damageCoefficient = enemyHealth.StartHealth / Damage;

            enemyHealth.HealthChanged += OnHealthChanged;
        }

        void OnHealthChanged(float currentHealth)
        {
            Damage = currentHealth / damageCoefficient;
        }

        void OnDestroy()
        {
            enemyHealth.HealthChanged -= OnHealthChanged;
        }
    }
}