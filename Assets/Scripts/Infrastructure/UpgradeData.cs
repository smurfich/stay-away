﻿namespace Infrastructure
{
    public struct UpgradeData
    {
        public int UpgradeCost;
        public float Value;
    }
}