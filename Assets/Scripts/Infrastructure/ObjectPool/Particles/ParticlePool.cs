﻿using Enemies;
using Infrastructure.Services.Factories.Particles;
using UnityEngine;

namespace Infrastructure.ObjectPool.Particles
{
	public class ParticlePool : IParticlePool
	{
		readonly IParticleFactory particleFactory;

		ParticleSystem tapParticle;
		ParticleSystem towerShootParticle;
		ParticleSystem bulletDamageParticle;
		ParticleSystem tapEnemyDeathParticle;
		ParticleSystem bulletEnemyDeathParticle;
		ParticleSystem zoneEnemyDeathParticle;
		ParticleSystem tapDamageParticle;
		ParticleSystem towerSpeedUpParticle;
		ParticleSystem towerDamageUpParticle;

		public ParticlePool(IParticleFactory particleFactory)
		{
			this.particleFactory = particleFactory;
			this.particleFactory.Load();
		}

		public ParticleSystem GetTapParticle()
		{
			if (tapParticle == null)
				CreateTapParticle();

			DisableParticle(tapParticle);

			return tapParticle;
		}

		public ParticleSystem GetBulletDamageParticle()
		{
			if (bulletDamageParticle == null)
				CreateBulletDamageParticle();

			DisableParticle(bulletDamageParticle);

			return bulletDamageParticle;
		}

		public ParticleSystem GetTapDamageParticle()
		{
			if (tapDamageParticle == null)
				CreateTapDamageParticle();

			DisableParticle(tapDamageParticle);

			return tapDamageParticle;
		}

		public ParticleSystem GetEnemyDeathParticle(DamageDealerType damageDealerType)
		{
			switch (damageDealerType)
			{
				case DamageDealerType.Touch:
					if (tapEnemyDeathParticle == null)
						CreateEnemyDeathParticle(DamageDealerType.Touch, out tapEnemyDeathParticle);
					DisableParticle(tapEnemyDeathParticle);

					return tapEnemyDeathParticle;
				case DamageDealerType.Bullet:
					if (bulletEnemyDeathParticle == null)
						CreateEnemyDeathParticle(DamageDealerType.Bullet, out bulletEnemyDeathParticle);
					DisableParticle(bulletEnemyDeathParticle);

					return bulletEnemyDeathParticle;
				case DamageDealerType.Zone:
					if (zoneEnemyDeathParticle == null)
						CreateEnemyDeathParticle(DamageDealerType.Zone, out zoneEnemyDeathParticle);
					DisableParticle(zoneEnemyDeathParticle);

					return zoneEnemyDeathParticle;
			}

			return null;
		}

		public ParticleSystem GetTowerShootParticle()
		{
			if (towerShootParticle == null)
				CreateTowerShootParticle();

			DisableParticle(towerShootParticle);

			return towerShootParticle;
		}

		public ParticleSystem GetTowerSpeedUpParticle()
		{
			if (towerSpeedUpParticle == null)
				CreateTowerSpeedUpParticle();

			DisableParticle(towerSpeedUpParticle);

			return towerSpeedUpParticle;
		}

		public ParticleSystem GetTowerDamageUpParticle()
		{
			if (towerDamageUpParticle == null)
				CreateTowerDamageUpParticle();

			DisableParticle(towerDamageUpParticle);

			return towerDamageUpParticle;
		}


		void CreateTapParticle() => tapParticle = particleFactory.CreateTapParticle();

		void CreateEnemyDeathParticle(DamageDealerType damageDealerType, out ParticleSystem deathParticle) =>
			deathParticle = particleFactory.CreateEnemyDeathParticle(damageDealerType);


		void CreateTowerShootParticle() => towerShootParticle = particleFactory.CreateTowerShootParticle();

		void CreateBulletDamageParticle() => bulletDamageParticle = particleFactory.CreateBulletDamageParticle();

		void CreateTapDamageParticle() => tapDamageParticle = particleFactory.CreateTapDamageParticle();

		void DisableParticle(ParticleSystem particle) => particle.Stop(true);
		void CreateTowerSpeedUpParticle() => towerSpeedUpParticle = particleFactory.CreateTowerSpeedUpParticle();
		void CreateTowerDamageUpParticle() => towerDamageUpParticle = particleFactory.CreateTowerDamageUpParticle();
	}
}