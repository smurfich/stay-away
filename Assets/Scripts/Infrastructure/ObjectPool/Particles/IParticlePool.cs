﻿using Enemies;
using UnityEngine;

namespace Infrastructure.ObjectPool.Particles
{
    public interface IParticlePool
    {
        ParticleSystem GetTapParticle();
        ParticleSystem GetEnemyDeathParticle(DamageDealerType damageDealerType);
        ParticleSystem GetTowerShootParticle();
        ParticleSystem GetBulletDamageParticle();
        ParticleSystem GetTapDamageParticle();
        ParticleSystem GetTowerSpeedUpParticle();
        ParticleSystem GetTowerDamageUpParticle();
    }
}