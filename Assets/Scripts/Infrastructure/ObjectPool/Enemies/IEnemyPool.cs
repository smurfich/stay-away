﻿using System;
using Infrastructure.Services.GoogleSheets;
using UnityEngine;

namespace Infrastructure.ObjectPool.Enemies
{
	public interface IEnemyPool
	{
		GameObject Get(EnemyType enemyType, Transform parent, Transform target, Transform parentCanvas);
		void Clear();
		event Action<GameObject> EnemyDied;
	}
}