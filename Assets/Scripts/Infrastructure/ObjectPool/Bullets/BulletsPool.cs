﻿using System.Collections.Generic;
using Infrastructure.ObjectPool.Enemies;
using Infrastructure.Services.Factories.Bullets;
using Logic;
using UnityEngine;

namespace Infrastructure.ObjectPool.Bullets
{
    public class BulletsPool : IBulletsPool
    {
        readonly IBulletsFactory bulletsFactory;
        readonly Queue<Bullet> bullets = new Queue<Bullet>();

        public BulletsPool(IBulletsFactory bulletsFactory)
        {
            this.bulletsFactory = bulletsFactory;
        }

        public void Load() => bulletsFactory.Load();

        public Bullet Get(Vector3 position, Transform parent)
        {
            if (bullets.Count == 0)
                CreateBullet(position, parent);

            return bullets.Dequeue();
        }

        public Bullet Get()
        {
            if (bullets.Count == 0)
                CreateBullet();

            return bullets.Dequeue();
        }

        public void Clear() => bullets.Clear();

        void ReturnToPool(Bullet bullet)
        {
            bullet.gameObject.SetActive(false);
            bullets.Enqueue(bullet);
        }

        void CreateBullet()
        {
            Bullet bullet = bulletsFactory.Create();
            bullet.Destroyed += ReturnToPool;
            bullet.gameObject.SetActive(false);

            bullets.Enqueue(bullet);
        }

        void CreateBullet(Vector3 position, Transform parent)
        {
            Bullet bullet = bulletsFactory.Create(position, parent);
            bullet.Destroyed += ReturnToPool;
            bullet.gameObject.SetActive(false);

            bullets.Enqueue(bullet);
        }
    }
}