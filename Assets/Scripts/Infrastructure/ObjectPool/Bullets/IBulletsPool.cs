﻿using Logic;
using UnityEngine;

namespace Infrastructure.ObjectPool.Bullets
{
    public interface IBulletsPool
    {
        Bullet Get();
        void Load();
        Bullet Get(Vector3 position, Transform parent);
        void Clear();
    }
}