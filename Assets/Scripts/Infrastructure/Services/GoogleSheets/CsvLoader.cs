﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;


namespace Infrastructure.Services.GoogleSheets
{
    public class CsvLoader : MonoBehaviour
    {
        [SerializeField] bool debug = true;
        [SerializeField] string sheetId;

        const string URL = "https://docs.google.com/spreadsheets/d/*/export?format=csv";

        const string SheetDataPath = "CVS Table/designData_";
        //const string SheetData = "designData_";


        public void DownloadTable(string[] gid, Action<string, string> onSheetLoadedAction)
        {
            string actualUrl = URL.Replace("*", sheetId);
            actualUrl += gid;

            foreach (string s in gid)
                StartCoroutine(DownloadCvsTable(actualUrl, s, onSheetLoadedAction));
        }


        IEnumerator DownloadCvsTable(string actualUrl, string gid, Action<string, string> callBack)
        {
            var textAsset = Resources.Load<TextAsset>(SheetDataPath + CsvData.CsvNames[gid]);
            callBack(textAsset.text, gid);

            yield break;

            // if (File.Exists(Application.dataPath + SheetData + CsvData.CsvNames[gid]))
            // {
            //     callBack(File.ReadAllText(Application.dataPath + SheetData + CsvData.CsvNames[gid]));
            //     yield break;
            // }
            //
            // using (UnityWebRequest request = UnityWebRequest.Get(actualUrl))
            // {
            //     yield return request.SendWebRequest();
            //
            //     if (request.isNetworkError || request.isHttpError)
            //     {
            //         Debug.LogError("Error");
            //     }
            //
            //     if (debug)
            //     {
            //         Debug.Log("Successful download");
            //         Debug.Log(request.downloadHandler.text);
            //     }
            //
            //     File.WriteAllText(Application.dataPath + SheetData + CsvData.CsvNames[gid],
            //         request.downloadHandler.text);
            //
            //     callBack(request.downloadHandler.text);
            // }
            //
            // yield return null;
        }
    }
}