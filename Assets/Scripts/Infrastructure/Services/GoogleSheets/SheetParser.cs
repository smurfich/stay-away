﻿using UnityEngine;

namespace Infrastructure.Services.GoogleSheets
{
	public class SheetParser : ISheetParser
	{
		const char InCellSeporator = ';';
		

		public string[] SplitTableRows(string rawData)
		{
			//char lineEnding = GetPlatformSpecificLineEnd();
			string[] rows = rawData.Split('\n');

			return rows;
		}

		public Vector3 ParseVector3(string s)
		{
			string[] vectorComponents = s.Split(InCellSeporator);
			if (vectorComponents.Length < 3)
			{
				Debug.Log("Can't parse Vector3. Wrong text format");
				return default;
			}

			float x = ParseFloat(vectorComponents[0]);
			float y = ParseFloat(vectorComponents[1]);
			float z = ParseFloat(vectorComponents[2]);
			return new Vector3(x, y, z);
		}

		public int ParseInt(string s)
		{
			if (!int.TryParse(s, System.Globalization.NumberStyles.Integer,
				    System.Globalization.CultureInfo.GetCultureInfo("en-US"), out int result))
			{
				Debug.Log("Can't parse int, wrong text");
			}

			return result;
		}

		public float ParseFloat(string s)
		{
			if (!float.TryParse(s, System.Globalization.NumberStyles.Any,
				    System.Globalization.CultureInfo.GetCultureInfo("en-US"), out float result))
			{
				Debug.Log("Can't pars float,wrong text ");
			}

			return result;
		}
		
		public float ParseFloatWithComma(string s)
		{
			string[] vectorComponents = s.Split(InCellSeporator);

			float first = ParseFloat(vectorComponents[0]);
			float second = ParseFloat(vectorComponents[1]);

			second /= 10;
			
			return first + second;
		}

		char GetPlatformSpecificLineEnd()
		{
			char lineEnding = '\n';
#if UNITY_IOS
        lineEnding = '\r';
#endif
			return lineEnding;
		}
	}
}