﻿using System.Collections.Generic;
using UnityEngine;

namespace Infrastructure.Services.GoogleSheets
{
	public class Enemy : MonoBehaviour
	{
		[SerializeField] EnemyType enemyType;

		Dictionary<EnemyType, string> enemyTypesId = new Dictionary<EnemyType, string>
		{
			{EnemyType.Small, "small_01"},
			{EnemyType.Medium, "medium_01"},
			{EnemyType.Big, "big_01"},
			{EnemyType.Fast, "fast_01"},
			{EnemyType.Giant, "giant"}
		};
	}

	public enum EnemyType
	{
		Small,
		Medium,
		Big,
		Fast,
		Giant,
	}
}