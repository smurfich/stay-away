﻿using System;
using UnityEngine;

namespace Infrastructure.Services.GoogleSheets
{
	public class SheetLoader : MonoBehaviour
	{
		public event Action<string> EnemiesDataTableLoaded;

		[SerializeField] CsvLoader csvLoader;
		[SerializeField] string sheetId;


		//void Start() => DownloadEnemiesDataTable();

		//void DownloadEnemiesDataTable() => csvLoader.DownloadTable(sheetId, string.Empty, OnSheetLoaded);

		void OnSheetLoaded(string result) => EnemiesDataTableLoaded?.Invoke(result);
	}
}