﻿using Logic;
using Logic.TowerBehaviour;
using Logic.UpgradeSystem.TowerUpgradeSystem;
using UnityEngine;

namespace Infrastructure.Services.Factories.TowerBuilding
{
    public interface ITowerFactory
    {
        void Load();
        Tower CreateTower(Vector3 spawnPoint, Transform parent);
        TowerUpgradePresenter CreateTowerUpgrade();
    }
}