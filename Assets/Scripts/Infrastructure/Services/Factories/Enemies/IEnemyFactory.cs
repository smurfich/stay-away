using System.Threading.Tasks;
using Infrastructure.Services.GoogleSheets;
using UnityEngine;

namespace Infrastructure.Services.Factories.Enemies
{
	public interface IEnemyFactory
	{
		Task<GameObject> CreateEnemyAsync(EnemyType enemyType, Transform parent, Transform target,
			Transform parentCanvas);

		GameObject CreateEnemy(EnemyType enemyType, Transform parent, Transform target, Transform parentCanvas);
	}
}