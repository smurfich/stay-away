﻿using Logic;
using UnityEngine;

namespace Infrastructure.Services.Factories.Bullets
{
    public interface IBulletsFactory
    {
        void Load();
        Bullet Create();
        Bullet Create(Vector3 position, Transform parent);
    }
}