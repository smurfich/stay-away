﻿using Framework.Code;
using Framework.Code.Infrastructure.Services.Assets;
using Logic;
using UnityEngine;
using Zenject;

namespace Infrastructure.Services.Factories.Bullets
{
    public class BulletsFactory : IBulletsFactory
    {
        readonly IAssetProvider assetProvider;
        readonly DiContainer container;

        Bullet bulletPrefab;

        public BulletsFactory(IAssetProvider assetProvider, DiContainer container)
        {
            this.assetProvider = assetProvider;
            this.container = container;
        }

        public void Load() => bulletPrefab = assetProvider.Load<Bullet>(AssetPath.Bullet);

        public Bullet Create() => container.InstantiatePrefabForComponent<Bullet>(bulletPrefab);

        public Bullet Create(Vector3 position, Transform parent) =>
            container.InstantiatePrefabForComponent<Bullet>(bulletPrefab, position, Quaternion.identity, parent);
    }
}