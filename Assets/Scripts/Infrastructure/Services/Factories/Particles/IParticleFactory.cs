﻿using Enemies;
using UnityEngine;

namespace Infrastructure.Services.Factories.Particles
{
    public interface IParticleFactory
    {
        void Load();
        ParticleSystem CreateTapParticle();
        ParticleSystem CreateEnemyDeathParticle(DamageDealerType damageDealerType);
        ParticleSystem CreateTowerShootParticle();
        ParticleSystem CreateBulletDamageParticle();
        ParticleSystem CreateTapDamageParticle();
        ParticleSystem CreateConfettiParticle();
        ParticleSystem CreateTowerSpeedUpParticle();
        ParticleSystem CreateTowerDamageUpParticle();
    }
}