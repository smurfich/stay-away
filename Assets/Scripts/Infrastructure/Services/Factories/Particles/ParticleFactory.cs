﻿using Enemies;
using Framework.Code;
using Framework.Code.Infrastructure.Services.Assets;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Infrastructure.Services.Factories.Particles
{
    public class ParticleFactory : IParticleFactory
    {
        readonly IAssetProvider assetProvider;

        ParticleSystem tapParticlePrefab;
        ParticleSystem bulletDamageParticlePrefab;
        ParticleSystem enemyDeathParticlePrefab;
        ParticleSystem deathFromBulletParticlePrefab;
        ParticleSystem deathFromZoneParticlePrefab;
        ParticleSystem towerShootParticlePrefab;
        ParticleSystem tapEnemyDamageParticlePrefab;
        ParticleSystem confettiParticlePrefab;
        ParticleSystem towerSpeedUpParticle;
        ParticleSystem towerDamageUpParticle;
        
        public ParticleFactory(IAssetProvider assetProvider)
        {
            this.assetProvider = assetProvider;
        }
        
        public void Load()
        {
            tapParticlePrefab = assetProvider.Load<ParticleSystem>(AssetPath.TapParticle);
            bulletDamageParticlePrefab = assetProvider.Load<ParticleSystem>(AssetPath.BulletEnemyDamageParticle);
            enemyDeathParticlePrefab = assetProvider.Load<ParticleSystem>(AssetPath.TouchEnemyDeathParticle);
            deathFromBulletParticlePrefab = assetProvider.Load<ParticleSystem>(AssetPath.BulletEnemyDeathParticle);
            deathFromZoneParticlePrefab = assetProvider.Load<ParticleSystem>(AssetPath.ZoneEnemyDeathParticle);
            towerShootParticlePrefab = assetProvider.Load<ParticleSystem>(AssetPath.TowerShootParticle);
            tapEnemyDamageParticlePrefab = assetProvider.Load<ParticleSystem>(AssetPath.TapEnemyDamageParticle);
            confettiParticlePrefab = assetProvider.Load<ParticleSystem>(AssetPath.ConfettiParticle);
            towerSpeedUpParticle = assetProvider.Load<ParticleSystem>(AssetPath.TowerSpeedUpParticle);
            towerDamageUpParticle = assetProvider.Load<ParticleSystem>(AssetPath.TowerDamageUpParticle);
        }

        public ParticleSystem CreateTapParticle() => Object.Instantiate(tapParticlePrefab);

        public ParticleSystem CreateBulletDamageParticle() => Object.Instantiate(bulletDamageParticlePrefab);
        public ParticleSystem CreateTapDamageParticle() => Object.Instantiate(tapEnemyDamageParticlePrefab);
        public ParticleSystem CreateEnemyDeathParticle(DamageDealerType damageDealerType)
        {
            switch (damageDealerType)
            {
                case DamageDealerType.Touch:
                    return Object.Instantiate(enemyDeathParticlePrefab);
                case DamageDealerType.Bullet:
                    return Object.Instantiate(deathFromBulletParticlePrefab);
                case DamageDealerType.Zone:
                    return Object.Instantiate(deathFromZoneParticlePrefab);
            }

            return null;
        }

        public ParticleSystem CreateTowerShootParticle() => Object.Instantiate(towerShootParticlePrefab);

        public ParticleSystem CreateConfettiParticle() => Object.Instantiate(confettiParticlePrefab);

        public ParticleSystem CreateTowerSpeedUpParticle() => Object.Instantiate(towerSpeedUpParticle);
        public ParticleSystem CreateTowerDamageUpParticle() => Object.Instantiate(towerDamageUpParticle);
    }
}