﻿using System.IO;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace Infrastructure.Services.Editor
{
    public static class CsvLoaderEditor
    {
        const string URL = "https://docs.google.com/spreadsheets/d/*/export?format=csv";

        const string SheetData = "/Resources/CVS Table/designData_";
        //const string SheetData = "designData_";

        [MenuItem("Google Sheets / Download")]
        public static async void DownloadCsv()
        {
            for (int i = 0; i < CsvData.CvsId.Length; i++)
            {
                string actualUrl = URL.Replace("*", "13Fv1X-B0em3vImHIX_U72a8BEY_uewN1GQpifSnE_is");
                actualUrl += CsvData.CvsId[i];

                using (UnityWebRequest request = UnityWebRequest.Get(actualUrl))
                {
                    request.SendWebRequest();

                    while (!request.isDone)
                    {
                        await Task.Yield();
                    }

                    if (request.isNetworkError || request.isHttpError)
                    {
                        Debug.LogError("Error");
                    }

                    if (request.isDone)
                    {
                        Debug.Log("Successful download");
                        Debug.Log(request.downloadHandler.text);


                        File.WriteAllText(Application.dataPath + SheetData + CsvData.CsvNames[CsvData.CvsId[i]] + ".txt",
                            request.downloadHandler.text);
                    }
                }
            }
            
            AssetDatabase.Refresh(ImportAssetOptions.ImportRecursive);
        }
    }
}