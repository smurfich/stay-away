﻿using System.Collections.Generic;
using System.Linq;
using Infrastructure.Data.EnemyWave;
using Infrastructure.Data.Zone;
using Infrastructure.Services.GoogleSheets;
using UnityEngine;
using UnityEngine.UI;

namespace Infrastructure
{
    public static class MethodsExtension
    {
        static readonly Dictionary<EnemyType, string> EnemyTypes = new Dictionary<EnemyType, string>
        {
            {EnemyType.Small, "small_01"},
            {EnemyType.Medium, "medium_01"},
            {EnemyType.Big, "big_01"},
            {EnemyType.Fast, "fast_01"},
            {EnemyType.Giant, "giant"}
        };

        static readonly Dictionary<ZoneType, string> ZoneTypes = new Dictionary<ZoneType, string>
        {
            {ZoneType.Simple, "zone_simple_01"},
            {ZoneType.Fast, "zone_fast_01"},
            {ZoneType.Standard, "zone_standard_01"},
        };

        static readonly Dictionary<WaveType, string> WaveTypes = new Dictionary<WaveType, string>
        {
            {WaveType.Tutor_01, "wave_tutor_01"},
            {WaveType.Easy_02, "wave_easy_02"},
            {WaveType.Level_03, "wave_level_03"},
            {WaveType.Level_04, "wave_level_04"},
            {WaveType.Level_05, "wave_level_05"}
        };

        public static void ClampYScale(this Transform transform, float y)
        {
            Vector3 scale = transform.localScale;
            scale.y = Mathf.Clamp(scale.y, y, y);
            transform.localScale = scale;
        }
        
        public static string GetEnemyTypeId(this EnemyType enemyType) => EnemyTypes[enemyType];

        public static string GetZoneTypeId(this ZoneType zoneType) => ZoneTypes[zoneType];

        public static EnemyType GetEnemyType(this string id) => EnemyTypes.FirstOrDefault(type => type.Value == id).Key;

        public static ZoneType GetZoneType(this string id) => ZoneTypes.FirstOrDefault(type => type.Value == id).Key;

        public static WaveType GetWaveType(this string id) => WaveTypes.FirstOrDefault(type => type.Value == id).Key;

        public static void SetScale(this Transform transform, float scale) =>
            transform.localScale = new Vector3(scale, scale, scale);

        public static void SetColorAlpha(this Graphic graphic, float alpha) =>
            graphic.color = new Color(graphic.color.r, graphic.color.g, graphic.color.b, alpha);
    }
}