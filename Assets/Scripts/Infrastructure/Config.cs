﻿namespace Infrastructure
{
	public static class Config
	{
		public const int ScaleCoefficient = 10;
		public const int PrimitivesScaleCoefficient = 5;
		public const int PrimitivesScaleDivider = 2;
		public static float RotationTimeDivider = 3;
		public const float FireworkDelay = 0.8f;
		public const float SwitchStateDelay = 1f;
	}
}