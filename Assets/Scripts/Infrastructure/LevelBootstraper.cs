﻿using Control;
using Enemies.Spawning;
using Framework.Code.Factories.Levels;
using Framework.Code.Infrastructure.Services.Analytics;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Framework.Code.Infrastructure.States;
using Infrastructure.Data.EnemyWave;
using Infrastructure.Data.Level;
using Infrastructure.Data.PlayerTap;
using Infrastructure.Data.Tower;
using Infrastructure.Data.Zone;
using Lean.Touch;
using Logic;
using Logic.Building;
using Logic.UI;
using Logic.UpgradeSystem.TapUpgradeSystem;
using PlayerZone;
using UnityEngine;
using Zenject;

namespace Infrastructure
{
	public class LevelBootstraper : MonoBehaviour
	{
		LevelData levelData;

		EnemyWaveDataProcessor waveDataProcessor;
		ZoneDataProcessor zoneDataProcessor;
		PlayerTapDataProcessor playerTapDataProcessor;
		TowerDataProcessor towerDataProcessor;
		IAnalyticsService analyticsService;
		IPersistentProgressService progressService;
		GameStateMachine gameStateMachine;

		EnemyWave enemyWave;
		Zone zone;
		ZoneBase zoneBase;
		TouchDetector touchDetector;
		LevelWinCondition levelWinCondition;
		TowerBuilder towerBuilder;
		TapUpgradePresenter tapUpgradePresenter;
		FirstLevelCanvas firstLevelCanvas;

		[Inject]
		void Construct(EnemyWaveDataProcessor waveDataProcessor, ZoneDataProcessor zoneDataProcessor,
			PlayerTapDataProcessor playerTapDataProcessor, TowerDataProcessor towerDataProcessor,
			IAnalyticsService analyticsService, IPersistentProgressService progressService,
			GameStateMachine gameStateMachine)
		{
			this.waveDataProcessor = waveDataProcessor;
			this.zoneDataProcessor = zoneDataProcessor;
			this.playerTapDataProcessor = playerTapDataProcessor;
			this.towerDataProcessor = towerDataProcessor;
			this.analyticsService = analyticsService;
			this.progressService = progressService;
			this.gameStateMachine = gameStateMachine;
		}

		public void Construct(LevelData levelData)
		{
			this.levelData = levelData;
		}

		public void EnablePreloadWindow()
		{
			InitTouchDetector();

			tapUpgradePresenter = GetComponentInChildren<TapUpgradePresenter>();
			if (tapUpgradePresenter == null)
			{
				LeanTouch.OnFingerDown += OnFingerDown;
				return;
			}

			tapUpgradePresenter.Construct(touchDetector);
			tapUpgradePresenter.Enable();
			tapUpgradePresenter.PlayButtonClicked += OnPlayButtonClicked;
		}

		public void InitZone()
		{
			zone = GetComponentInChildren<Zone>();
			zoneBase = GetComponentInChildren<ZoneBase>();

			ZoneData zoneData = zoneDataProcessor.Load(levelData.ZoneType);

			zone.Construct(zoneData);
			zone.Enable();
			zoneBase.SetRadius(zoneData.DeathRadius);
		}

		void OnFingerDown(LeanFinger finger)
		{
			BootstrapLevel();

			EnableFirstLevelHints();

			LeanTouch.OnFingerDown -= OnFingerDown;
		}

		void OnPlayButtonClicked() => BootstrapLevel();

		void BootstrapLevel()
		{
			var level = GetComponent<Level>();

			analyticsService.LevelStarted(level.Id, progressService.Progress.Level);
			InitEntities();

			if (tapUpgradePresenter != null)
			{
				tapUpgradePresenter.PlayButtonClicked -= OnPlayButtonClicked;
				tapUpgradePresenter.Disable();
			}

			gameStateMachine.Enter<GameLoopState, Level>(level);
		}


		void EnableFirstLevelHints()
		{
			firstLevelCanvas = GetComponentInChildren<FirstLevelCanvas>();
			firstLevelCanvas.DisableTapToStartText();
			firstLevelCanvas.EnableTapOnTheFireText();
		}


		void InitTouchDetector()
		{
			touchDetector = GetComponentInChildren<TouchDetector>();
			PlayerTapData tapData = playerTapDataProcessor.Load();
			touchDetector.Construct(tapData.AreaValue, tapData.DamageValue);
		}

		void InitEntities()
		{
			enemyWave = GetComponentInChildren<EnemyWave>();
			levelWinCondition = GetComponentInChildren<LevelWinCondition>();
			towerBuilder = GetComponentInChildren<TowerBuilder>();
			ILevelEnder[] levelEnders = GetComponentsInChildren<ILevelEnder>();

			ZoneData zoneData = zoneDataProcessor.Load(levelData.ZoneType);
			TowerData towerData = towerDataProcessor.Load();

			enemyWave.Construct(waveDataProcessor.Load(levelData.WaveType));
			levelWinCondition.Construct(zone, levelData.Reward, zoneData.EndRadius, zoneData.DeathRadius, levelEnders);
			towerBuilder.Construct(towerData, zoneData.DeathRadius);

			enemyWave.Enable();
			levelWinCondition.Enable();
			towerBuilder.Enable();
			touchDetector.Enable();
		}

		public void DisableEntities()
		{
			enemyWave.Disable();
			levelWinCondition.Disable();
			towerBuilder.Disable();
			touchDetector.Disable();
			zone.Disable();

			if (tapUpgradePresenter != null)
				tapUpgradePresenter.Disable();
		}
	}
}