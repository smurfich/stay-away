﻿using Framework.Code.Infrastructure.Services.Assets;
using Infrastructure.Services.GoogleSheets;

namespace Infrastructure.Data
{
    public abstract class DataProcessor
    {
        protected const char Separator = ',';

        readonly CsvLoader csvLoader;

        
        public abstract bool NeedRefresh { get; }
        protected abstract string[] AllTableId { get; }
        
        protected IAssetProvider AssetProvider { get; }
        protected ISheetParser SheetParser { get; }


        public DataProcessor(CsvLoader csvLoader, IAssetProvider assetProvider, ISheetParser sheetParser)
        {
            this.csvLoader = csvLoader;
            AssetProvider = assetProvider;
            SheetParser = sheetParser;
        }

        public virtual void Init() => csvLoader.DownloadTable(AllTableId, OnDataLoaded);

        protected abstract void OnDataLoaded(string result, string gid);
    }
}