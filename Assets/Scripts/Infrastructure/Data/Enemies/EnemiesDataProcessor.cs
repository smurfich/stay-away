﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Code;
using Framework.Code.Infrastructure.Services.Assets;
using Infrastructure.Services.GoogleSheets;

namespace Infrastructure.Data.Enemies
{
	public class EnemiesDataProcessor : DataProcessor
	{
		const int ID = 0;
		const int Health = 1;
		const int Speed = 2;
		const int Damage = 3;
		const int Scale = 4;
		const int Reward = 5;
		public override bool NeedRefresh => false;
		protected override string[] AllTableId => new[] {""};

		EnemyData[] enemiesData;

		bool isLoaded;

		public EnemiesDataProcessor(CsvLoader csvLoader, IAssetProvider assetProvider, ISheetParser sheetParser) : base(
			csvLoader, assetProvider, sheetParser)
		{
		}

		public override void Init()
		{
			enemiesData = AssetProvider.LoadCollection<EnemyData>(AssetPath.EnemiesData);

			base.Init();
		}


		protected override void OnDataLoaded(string result, string gid)
		{
			string[] rows = SheetParser.SplitTableRows(result);

			List<EnemyData> tempEnemiesData = new List<EnemyData>();
			tempEnemiesData.AddRange(enemiesData);

			for (int i = 1; i < rows.Length; i++)
			{
				string[] cells = rows[i].Split(Separator);
				string id = cells[ID].Trim();

				EnemyData enemyData = tempEnemiesData.FirstOrDefault(data => data.EnemyType.GetEnemyTypeId() == id);
				if (enemyData == null) return;

				tempEnemiesData.Remove(enemyData);

				float health = SheetParser.ParseFloat(cells[Health]);
				float speed = SheetParser.ParseFloat(cells[Speed]);
				float damage = SheetParser.ParseFloat(cells[Damage]);
				float scale = SheetParser.ParseFloat(cells[Scale]);
				int reward = SheetParser.ParseInt(cells[Reward]);

				enemyData.StartHealth = health;
				enemyData.StartSpeed = speed;
				enemyData.StartDamage = damage;
				enemyData.StartScale = scale;
				enemyData.Reward = reward;
			}

			isLoaded = true;
		}


		public EnemyData Load(EnemyType enemyType) => enemiesData.First(data => data.EnemyType == enemyType);

		public async Task<EnemyData> LoadAsync(EnemyType enemyType)
		{
			while (!isLoaded)
			{
				await Task.Yield();
			}

			return enemiesData.First(data => data.EnemyType == enemyType);
		}
	}
}