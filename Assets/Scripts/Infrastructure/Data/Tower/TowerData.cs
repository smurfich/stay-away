﻿using UnityEngine;

namespace Infrastructure.Data.Tower
{
    [CreateAssetMenu(fileName = "Tower Data", menuName = "Design / Tower Data")]
    public class TowerData : ScriptableObject
    {
        public float FrequencyValue;
        public int FrequencyCost;
        public float DamageValue;
        public int DamageCost;
    }
}