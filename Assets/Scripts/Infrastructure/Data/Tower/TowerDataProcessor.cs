﻿using Framework.Code;
using Framework.Code.Infrastructure.Services.Assets;
using Infrastructure.Services.GoogleSheets;
using UnityEngine;

namespace Infrastructure.Data.Tower
{
    public class TowerDataProcessor : DataProcessor
    {
        const int Level = 0;
        const int FrequencyValue = 1;
        const int FrequencyCost = 2;
        const int DamageValue = 3;
        const int DamageCost = 4;

        public override bool NeedRefresh => true;
        protected override string[] AllTableId => new[] {"&gid=391672533"};

        TowerData towerData;


        bool isParsed;

        string rawData;


        public TowerDataProcessor(CsvLoader csvLoader, IAssetProvider assetProvider, ISheetParser sheetParser) : base(
            csvLoader, assetProvider, sheetParser)
        {
        }


        public override void Init()
        {
            towerData = AssetProvider.Load<TowerData>(AssetPath.TowerData);

            base.Init();
        }


        protected override void OnDataLoaded(string result, string gid)
        {
            rawData = result;
            ParseData(result);
        }

        void ParseData(string result, int upgradeLevel = 0)
        {
            string[] rows = SheetParser.SplitTableRows(result);

            for (int i = 1; i < rows.Length; i++)
            {
                string[] cells = rows[i].Split(Separator);

                int level = SheetParser.ParseInt(cells[Level]);
                if (upgradeLevel == level)
                {
                    float frequencyValue = SheetParser.ParseFloat(cells[FrequencyValue]);
                    int frequencyCost = SheetParser.ParseInt(cells[FrequencyCost]);
                    float damageValue = SheetParser.ParseFloat(cells[DamageValue]);
                    int damageCost = SheetParser.ParseInt(cells[DamageCost]);

                    towerData.FrequencyValue = frequencyValue;
                    towerData.FrequencyCost = frequencyCost;
                    towerData.DamageValue = damageValue;
                    towerData.DamageCost = damageCost;

                    break;
                }
            }
        }

        public UpgradeData GetFrequencyUpgradeData(int upgradeLevel)
        {
            string[] rows = SheetParser.SplitTableRows(rawData);

            if (upgradeLevel >= rows.Length - 1) return default;
            
            var upgradeData = new UpgradeData();

            for (int i = 1; i < rows.Length; i++)
            {
                string[] cells = rows[i].Split(Separator);

                int level = SheetParser.ParseInt(cells[Level]);

                if (upgradeLevel == level)
                {
                    float frequencyValue = SheetParser.ParseFloat(cells[FrequencyValue]);
                    int frequencyCost = SheetParser.ParseInt(cells[FrequencyCost]);

                    upgradeData.Value = frequencyValue;
                    upgradeData.UpgradeCost = frequencyCost;

                    break;
                }
            }

            return upgradeData;
        }

        public UpgradeData GetDamageUpgradeData(int upgradeLevel)
        {
            string[] rows = SheetParser.SplitTableRows(rawData);

            if (upgradeLevel >= rows.Length - 1) return default;
            
            var upgradeData = new UpgradeData();

            for (int i = 1; i < rows.Length; i++)
            {
                string[] cells = rows[i].Split(Separator);

                int level = SheetParser.ParseInt(cells[Level]);
                if (upgradeLevel == level)
                {
                    float damageValue = SheetParser.ParseFloat(cells[DamageValue]);
                    int damageCost = SheetParser.ParseInt(cells[DamageCost]);

                    upgradeData.Value = damageValue;
                    upgradeData.UpgradeCost = damageCost;

                    break;
                }
            }

            return upgradeData;
        }

        public TowerData Load() => towerData;
    }
}