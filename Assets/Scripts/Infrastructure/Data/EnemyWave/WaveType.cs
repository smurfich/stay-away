﻿namespace Infrastructure.Data.EnemyWave
{
    public enum WaveType
    {
        Tutor_01,
        Easy_02,
        Level_03,
        Level_04,
        Level_05
    }
}