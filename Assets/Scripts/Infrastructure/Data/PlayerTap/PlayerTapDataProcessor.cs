﻿using System.Threading.Tasks;
using Framework.Code;
using Framework.Code.Infrastructure.Services.Assets;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Infrastructure.Services.GoogleSheets;

namespace Infrastructure.Data.PlayerTap
{
	public class PlayerTapDataProcessor : DataProcessor
	{
		const int Level = 0;
		const int AreaValue = 1;
		const int AreaCost = 2;
		const int DamageValue = 3;
		const int DamageCost = 4;

		public override bool NeedRefresh => false;
		protected override string[] AllTableId => new[] {"&gid=498965198"};

		PlayerTapData tapData;
		readonly IPersistentProgressService progressService;
		
		bool isParsed;
		bool isLoaded;

		string rawData;


		public PlayerTapDataProcessor(CsvLoader csvLoader, IAssetProvider assetProvider, ISheetParser sheetParser, IPersistentProgressService progressService) :
			base(csvLoader, assetProvider,
				sheetParser)
		{
			this.progressService = progressService;
		}

		public override void Init()
		{
			tapData = AssetProvider.Load<PlayerTapData>(AssetPath.TapData);
			base.Init();
		}

		protected override void OnDataLoaded(string result, string gid)
		{
			rawData = result;
			ParseData(result);
		}

		void ParseData(string result)
		{
			string[] rows = SheetParser.SplitTableRows(result);

			for (int i = 1; i < rows.Length; i++)
			{
				string[] cells = rows[i].Split(Separator);

				int level = SheetParser.ParseInt(cells[Level]);
				
				if (progressService.Progress.TapUpgrade.AreaUpgrade == level)
				{
					float areaValue = SheetParser.ParseFloat(cells[AreaValue]);
					int areaCost = SheetParser.ParseInt(cells[AreaCost]);
					
					tapData.AreaValue = areaValue;
					tapData.AreaCost = areaCost;
				}

				if (progressService.Progress.TapUpgrade.DamageUpgrade == level)
				{
					float damageValue = SheetParser.ParseFloat(cells[DamageValue]);
					int damageCost = SheetParser.ParseInt(cells[DamageCost]);
					
					tapData.DamageValue = damageValue;
					tapData.DamageCost = damageCost;
				}

			}

			isParsed = true;
			isLoaded = true;
		}

		public UpgradeData GetAreaUpgradeData(int upgradeLevel)
		{
			string[] rows = SheetParser.SplitTableRows(rawData);
			if (upgradeLevel >= rows.Length - 1) return default;

			var upgradeData = new UpgradeData();

			for (int i = 1; i < rows.Length; i++)
			{
				string[] cells = rows[i].Split(Separator);

				int level = SheetParser.ParseInt(cells[Level]);
				if (upgradeLevel == level)
				{
					float areaValue = SheetParser.ParseFloat(cells[AreaValue]);
					int areaCost = SheetParser.ParseInt(cells[AreaCost]);

					upgradeData.Value = areaValue;
					upgradeData.UpgradeCost = areaCost;

					break;
				}
			}

			return upgradeData;
		}

		public UpgradeData GetDamageUpgradeData(int upgradeLevel)
		{
			string[] rows = SheetParser.SplitTableRows(rawData);
			if (upgradeLevel >= rows.Length - 1) return default;

			var upgradeData = new UpgradeData();

			for (int i = 1; i < rows.Length; i++)
			{
				string[] cells = rows[i].Split(Separator);

				int level = SheetParser.ParseInt(cells[Level]);
				if (upgradeLevel == level)
				{
					float damageValue = SheetParser.ParseFloat(cells[DamageValue]);
					int damageCost = SheetParser.ParseInt(cells[DamageCost]);

					upgradeData.Value = damageValue;
					upgradeData.UpgradeCost = damageCost;

					break;
				}
			}

			return upgradeData;
		}


		public async Task<PlayerTapData> LoadAsync()
		{
			while (!isLoaded)
			{
				await Task.Yield();
			}

			return tapData;
		}

		public PlayerTapData Load() => tapData;
	}
}