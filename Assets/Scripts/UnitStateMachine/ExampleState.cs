﻿using UnityEngine;

public class ExampleState : IState
{
    readonly int exampleParam;
    
    public ExampleState(int exampleParam)
    {
        this.exampleParam = exampleParam;
    }
    public void Tick()
    {
        
    }

    public void PhysicsTick()
    {
        
    }

    public void OnEnter()
    {
        
    }

    public void OnExit()
    {
        
    }
}