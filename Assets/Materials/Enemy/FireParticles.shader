﻿Shader "Unlit/FireParticles"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _MainColor ("Color", Color) = (1,1,1,1)
        _ArangeX ("Max fluctuation X", float) = 0.1
        _ArangeZ ("Max fluctuation Z", float) = 0.1
        _Speed ("Speed of fluctuation", float) = 1
        _Frequency ("Frequency", float) = 0.25
        
        [Space]
        _Height2("Particles vertex height", Float) = 10
        _P_Amplitude_V("Vertical amplitude", Range(0, 10)) = 5
        _P_Amplitude_H("Horizontal amplitude", Range(0, 15)) = 8
        _ParticlesSpeed("Particles speed", Range(0, 10)) = 5
        _ParticlesDelay("Particles delay", Range(1, 10)) = 5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };
            
            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            UNITY_INSTANCING_BUFFER_START(Props)
                UNITY_DEFINE_INSTANCED_PROP(float4, _MainColor)
                UNITY_DEFINE_INSTANCED_PROP(float, _ArangeX)
                UNITY_DEFINE_INSTANCED_PROP(float, _ArangeZ)
                UNITY_DEFINE_INSTANCED_PROP(float, _Speed)
                UNITY_DEFINE_INSTANCED_PROP(float, _Frequency)
                UNITY_DEFINE_INSTANCED_PROP(float, _Height2)
                UNITY_DEFINE_INSTANCED_PROP(float, _P_Amplitude_V)
                UNITY_DEFINE_INSTANCED_PROP(float, _P_Amplitude_H)
                UNITY_DEFINE_INSTANCED_PROP(float, _ParticlesSpeed)
                UNITY_DEFINE_INSTANCED_PROP(float, _ParticlesDelay)
            UNITY_INSTANCING_BUFFER_END(Props)

            sampler2D _MainTex;
            float4 _MainTex_ST;
            
            float ease_in_cubic(float value) {
                return value * value * value;
            }

            v2f vert (appdata v)
            {
                v2f o;

                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o);

                v.vertex.x = v.vertex.x + UNITY_ACCESS_INSTANCED_PROP(Props, _ArangeX)*v.vertex.z*sin(_Time.y * UNITY_ACCESS_INSTANCED_PROP(Props, _Speed) + v.vertex.z * UNITY_ACCESS_INSTANCED_PROP(Props, _Frequency));
                v.vertex.y = v.vertex.y + UNITY_ACCESS_INSTANCED_PROP(Props, _ArangeZ)*v.vertex.z*sin(_Time.y * UNITY_ACCESS_INSTANCED_PROP(Props, _Speed) + v.vertex.z * UNITY_ACCESS_INSTANCED_PROP(Props, _Frequency));   

                // Pause
                v.vertex.xy *= saturate(-(UNITY_ACCESS_INSTANCED_PROP(Props, _ParticlesDelay) - 1) + fmod(_Time.y * UNITY_ACCESS_INSTANCED_PROP(Props, _ParticlesSpeed) * 0.4, UNITY_ACCESS_INSTANCED_PROP(Props, _ParticlesDelay)));
                // Move up
                v.vertex.z += -UNITY_ACCESS_INSTANCED_PROP(Props, _Height2) + fmod(_Time.y * UNITY_ACCESS_INSTANCED_PROP(Props, _ParticlesSpeed) * 0.4, 1) * UNITY_ACCESS_INSTANCED_PROP(Props, _P_Amplitude_V) * 0.125;
                // Move sideways and scale to 0
                v.vertex.xy *= (1 - fmod(_Time.y * UNITY_ACCESS_INSTANCED_PROP(Props, _ParticlesSpeed) * 0.4, 1)) * UNITY_ACCESS_INSTANCED_PROP(Props, _P_Amplitude_H);
                
                /*half hDistance = sqrt(length(v.vertex.xy) * 1000 * UNITY_ACCESS_INSTANCED_PROP(Props, _IntensityV1Variance1) + 0.5);
                half mod = UNITY_ACCESS_INSTANCED_PROP(Props, _SpeedV1) * _Time.y + length(v.vertex.xy) * 1000 * UNITY_ACCESS_INSTANCED_PROP(Props, _IntensityV1Variance2);
			    if (v.vertex.z > 0.002)
			    {
			        v.vertex.z += sin(mod) / hDistance * UNITY_ACCESS_INSTANCED_PROP(Props, _IntensityV1) * 0.05;
			    }*/
                
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);
                // sample the texture
                float4 col = tex2D(_MainTex, i.uv) * UNITY_ACCESS_INSTANCED_PROP(Props, _MainColor);
                return col;
            }
            ENDCG
        }
    }
}
