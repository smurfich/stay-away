﻿using System;
using Framework.Code.Infrastructure.States;
using Infrastructure;
using Infrastructure.Data.EnemyWave;
using UnityEngine;
using Zenject;

namespace Framework.Code.Infrastructure
{
	public class GameBootstrapper : MonoBehaviour
	{
		[SerializeField] string test;
		[SerializeField] WaveType waveType;

		GameStateMachine stateMachine;

		[ContextMenu("Test")]
		void Test()
		{
			waveType = test.GetWaveType();
		}

		[Inject]
		void Construct(GameStateMachine stateMachine)
		{
			this.stateMachine = stateMachine;
		}

		void Awake()
		{
			Application.targetFrameRate = 60;
			stateMachine.Enter<BootstrapState>();
		}

		[ContextMenu("Next level")]
		void ToNextLevel()
		{
			stateMachine.Enter<WinState>();
		}
	}
}