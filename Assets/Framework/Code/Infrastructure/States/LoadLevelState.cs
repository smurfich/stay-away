using System.Linq;
using Framework.Code.Factories.Levels;
using Framework.Code.Infrastructure.Services.Analytics;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Framework.Code.UI;
using Framework.Code.UI.Elements;
using Infrastructure;
using Infrastructure.Data;
using Infrastructure.Data.Level;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Framework.Code.Infrastructure.States
{
	public class LoadLevelState : IState
	{
		readonly GameStateMachine stateMachine;
		readonly ILevelFactory levelFactory;
		readonly WindowPool windowPool;
		readonly UIRoot uiRoot;
		readonly IPersistentProgressService progressService;
		readonly IAnalyticsService analyticsService;
		readonly DataProcessor[] dataProcessors;

		Level currentLevel;

		bool dataIsParsed;

		public LoadLevelState(GameStateMachine stateMachine, ILevelFactory levelFactory, WindowPool windowPool,
			UIRoot uiRoot,
			IPersistentProgressService progressService, IAnalyticsService analyticsService,
			DataProcessor[] dataProcessors)
		{
			this.stateMachine = stateMachine;
			this.levelFactory = levelFactory;
			this.windowPool = windowPool;
			this.uiRoot = uiRoot;
			this.progressService = progressService;
			this.analyticsService = analyticsService;
			this.dataProcessors = dataProcessors;

			this.levelFactory.Load();
		}

		public void Enter()
		{
			UpdateUI();
			InitLevel();
		}

		public void Exit()
		{
			windowPool.EnableWindows(WindowType.LevelCoins);
		}


		void UpdateUI()
		{
			windowPool.DisableAllWindows();

			foreach (IViewUpdatable updater in uiRoot.ViewUpdaters)
			{
				updater.UpdateView();
			}
		}

		void InitLevel()
		{
			if (currentLevel != null)
				Object.Destroy(currentLevel.gameObject);

			currentLevel = levelFactory.Create();

			if (currentLevel != null)
			{
				SendAnalytics();
				InitDataProcessors();
				InitZone();
				EnablePreloadWindow();
			}
			else Debug.LogError("Level is not loaded");
		}

		void SendAnalytics()
		{
			analyticsService.LevelLoaded(currentLevel.Id, progressService.Progress.Level);
		}

		void InitDataProcessors()
		{
			if (dataIsParsed == false)
			{
				foreach (DataProcessor dataProcessor in dataProcessors)
				{
					dataProcessor.Init();
				}

				dataIsParsed = true;
			}
			else
			{
				foreach (DataProcessor dataProcessor in dataProcessors.Where(processor => processor.NeedRefresh))
				{
					dataProcessor.Init();
				}
			}
		}

		void EnablePreloadWindow() => currentLevel.GetComponent<LevelBootstraper>().EnablePreloadWindow();

		void InitZone()
		{
			var levelDataProcessor =
				(LevelDataProcessor) dataProcessors.First(processor => processor is LevelDataProcessor);

			LevelData data = levelDataProcessor.Load();
			currentLevel.GetComponent<LevelBootstraper>().Construct(data);

			currentLevel.GetComponent<LevelBootstraper>().InitZone();
		}
	}
}