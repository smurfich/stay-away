﻿using System;
using System.Collections.Generic;
using Framework.Code.Factories.Levels;
using Framework.Code.Infrastructure.Services.Analytics;
using Framework.Code.Infrastructure.Services.Assets;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Framework.Code.Infrastructure.Services.SaveSystem;
using Framework.Code.Infrastructure.Signals;
using Framework.Code.UI;
using Framework.Code.UI.Elements;
using Infrastructure.Data;
using Infrastructure.Data.Enemies;
using Infrastructure.Data.EnemyWave;
using Infrastructure.Data.Level;
using Infrastructure.Data.PlayerTap;
using Infrastructure.Data.Tower;
using Infrastructure.Data.Zone;
using Logic.UI;
using Zenject;

namespace Framework.Code.Infrastructure.States
{
	public class GameStateMachine
	{
		public IBaseState ActiveState { get; private set; }

		readonly Dictionary<Type, IBaseState> states;
		readonly SignalBus signalBus;

		public GameStateMachine(ISaveLoadService saveLoad, IPersistentProgressService progressService,
			ILevelFactory levelFactory, WindowPool windowPool,
			UIRoot uiRoot, IAnalyticsService analyticsService, IAssetProvider assetProvider, SignalBus signalBus,
			EnemiesDataProcessor enemiesDataProcessor,
			EnemyWaveDataProcessor waveDataProcessor, LevelDataProcessor levelDataProcessor,
			PlayerTapDataProcessor playerTapDataProcessor, TowerDataProcessor towerDataProcessor,
			ZoneDataProcessor zoneDataProcessor, TotalCoinsPassiveView totalCoinsPassiveView)
		{
			DataProcessor[] dataProcessors =
			{
				enemiesDataProcessor, waveDataProcessor, levelDataProcessor, playerTapDataProcessor, towerDataProcessor,
				zoneDataProcessor
			};

			this.signalBus = signalBus;
			states = new Dictionary<Type, IBaseState>
			{
				{typeof(BootstrapState), new BootstrapState(this, assetProvider)},
				{
					typeof(LoadLevelState),
					new LoadLevelState(this, levelFactory, windowPool, uiRoot, progressService,
						analyticsService, dataProcessors)
				},
				{typeof(LoadProgressState), new LoadProgressState(this, progressService, saveLoad)},
				{typeof(GameLoopState), new GameLoopState()},
				{
					typeof(WinState),
					new WinState(this, windowPool, totalCoinsPassiveView, progressService, saveLoad, analyticsService,
						levelFactory,
						assetProvider)
				},
				{
					typeof(LoseState),
					new LoseState(windowPool, this, analyticsService, progressService, levelFactory, assetProvider,
						totalCoinsPassiveView)
				}
			};
		}

		public void Enter<TState>() where TState : class, IState
		{
			TState state = ChangeState<TState>();
			state.Enter();

			signalBus?.Fire(new StateChangedSignal {State = state});
		}

		public void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload>
		{
			TState state = ChangeState<TState>();
			state.Enter(payload);

			signalBus?.Fire(new StateChangedSignal {State = state});
		}

		TState ChangeState<TState>() where TState : class, IBaseState
		{
			ActiveState?.Exit();

			TState nextState = states[typeof(TState)] as TState;
			ActiveState = nextState;

			return nextState;
		}
	}
}