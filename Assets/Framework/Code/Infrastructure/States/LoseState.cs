﻿using Framework.Code.Data;
using Framework.Code.Factories.Levels;
using Framework.Code.Infrastructure.Services.Analytics;
using Framework.Code.Infrastructure.Services.Assets;
using Framework.Code.Infrastructure.Services.PersistentProgress;
using Framework.Code.UI;
using Logic.UI;
using UnityEngine.UI;

namespace Framework.Code.Infrastructure.States
{
	public class LoseState : IState
	{
		readonly WindowPool windowPool;
		readonly GameStateMachine stateMachine;
		readonly IAnalyticsService analyticsService;
		readonly IPersistentProgressService progressService;
		readonly ILevelFactory levelFactory;
		readonly TotalCoinsPassiveView totalCoinsPassiveView;
		readonly GameData gameData;

		Button restartButton;

		public LoseState(WindowPool windowPool, GameStateMachine stateMachine, IAnalyticsService analyticsService,
			IPersistentProgressService progressService, ILevelFactory levelFactory, IAssetProvider assetProvider,
			TotalCoinsPassiveView totalCoinsPassiveView)
		{
			this.windowPool = windowPool;
			this.stateMachine = stateMachine;
			this.analyticsService = analyticsService;
			this.progressService = progressService;
			this.levelFactory = levelFactory;
			this.totalCoinsPassiveView = totalCoinsPassiveView;

			gameData = assetProvider.Load<GameData>(AssetPath.GAME_DATA);
		}

		public void Enter()
		{
			UpdateUI();
			SubscribeOnButtonClick();

			ResetCollectablesProgress();
			analyticsService.LevelCompleted(levelFactory.CurrentLevel.Id, progressService.Progress.Level, false,
				progressService.Progress.Collectables.LevelAmount, levelFactory.CurrentLevel.TimeSpent);
		}


		public void Exit()
		{
			if (restartButton != null)
				restartButton.onClick.RemoveListener(SwitchState);
		}

		void SubscribeOnButtonClick()
		{
			Graphic losePanel = windowPool.GetWindow(WindowType.Lose);
			restartButton = losePanel.GetComponentInChildren<Button>();

			if (restartButton != null)
				restartButton.onClick.AddListener(SwitchState);
		}


		void UpdateUI()
		{
			windowPool.DisableWindows(WindowType.LevelCoins);
			windowPool.EnableWindows(WindowType.Lose);
			totalCoinsPassiveView.UpdateLevelReward(progressService.Progress.Collectables.LevelAmount);
			totalCoinsPassiveView.UpdateTotalRewardText(progressService.Progress.Collectables.Amount);
		}

		void SwitchState()
		{
			stateMachine.Enter<LoadLevelState>();
		}

		void ResetCollectablesProgress()
		{
			progressService.Progress.Collectables.LevelAmount = 0;
			progressService.Progress.Collectables.FireCoinsAmount = 0;
		}
	}
}