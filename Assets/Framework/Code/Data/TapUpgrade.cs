﻿using System;

namespace Framework.Code.Data
{
	[Serializable]
	public class TapUpgrade
	{
		public int AreaUpgrade;
		public int DamageUpgrade;
	}
}