﻿using System;

namespace Framework.Code.Data
{
	[Serializable]
	public class CollectablesData
	{
		public event Action AmountChanged;
		public event Action FireAmountChanged;
		
		[NonSerialized] public int LevelAmount;
		[NonSerialized] public int FireCoinsAmount;
		
		public int Amount;

		public void AddFireCoins(int amount)
		{
			FireCoinsAmount += amount;
			FireAmountChanged?.Invoke();
		}

		public void TakeAwayFireCoins(int amount)
		{
			FireCoinsAmount -= amount;
			FireAmountChanged?.Invoke();
		}
		
		public void Add(int amount)
		{
			LevelAmount += amount;
			Amount += amount;

			AmountChanged?.Invoke();
		}

		public void TakeAwayCoins(int amount)
		{
			Amount -= amount;
			
			AmountChanged?.Invoke();
		}

		public void ResetAmount() => Amount -= LevelAmount;
	}
}