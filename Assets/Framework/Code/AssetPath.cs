﻿namespace Framework.Code
{
    public static class AssetPath
    {
        public const string TUTORIAL_LEVELS = "Levels";
        public const string LEVELS = "Levels";
        public const string LEVELS_DATABASE = "LevelDatabase";
        public const string GAME_DATA = "GameData";
        public const string EnemiesData = "Data/Enemies";
        public const string TapData = "Data/Tap/Player Tap Data";
        public const string EnemyWave = "Data/Wave";
        public const string ZoneData = "Data/Zone";
        public const string TowerData = "Data/Tower/Tower Data";
        public const string LevelData = "Data/Level/Level Data";
        public const string Bullet = "Prefabs/Bullet";
        public const string Tower = "Prefabs/Tower";
        public const string TowerUpgrade = "Prefabs/Tower Upgrade";
        public const string TapParticle = "Particles/Tap Particle";
        public const string TowerShootParticle = "Particles/Tower Shoot Particle";
        public const string TouchEnemyDeathParticle = "Particles/Touch Enemy Death Particle";
        public const string BulletEnemyDamageParticle = "Particles/Bullet Enemy Damage Particle";
        public const string BulletEnemyDeathParticle = "Particles/Bullet Enemy Death Particle";
        public const string ZoneEnemyDeathParticle = "Particles/Zone Enemy Death Particle";
        public const string TapEnemyDamageParticle = "Particles/Touch Enemy Damage Particle";
        public const string ConfettiParticle = "Particles/Confetti";
        public const string TowerSpeedUpParticle = "Particles/Tower Speed Up";
        public const string TowerDamageUpParticle = "Particles/Tower Damage Up";
    }
}